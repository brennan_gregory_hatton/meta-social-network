﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Meta;

namespace SiliconVagabond
{
	
	public class LogoAnimation : MonoBehaviour {
		
		static bool finished = false;
		
		float speed = 0;
		public float maxSpeed = 0.1f;
		public float speedConstant = 1;
		public bool DestroyOnZero = false;
		public Vector3 scaleTarget;
		
		public List<GameObject> triggerOn;
		public List<GameObject> triggerOff;
		
	// Use this for initialization
		void Start () {
			if (scaleTarget == Vector3.zero)
				scaleTarget = this.transform.localScale;
		}
		
	// Update is called once per frame
		void Update () {
			transform.position = Vector3.MoveTowards(this.transform.position, Vector3.zero, speed);
			this.transform.localScale = Vector3.MoveTowards(this.transform.localScale, scaleTarget, Mathf.Abs(speed));
			
			if (MetaCore.Instance.initialized)
			{
				speed += Mathf.Min(Mathf.Max(speed,0.001f),maxSpeed)*Time.deltaTime*speedConstant;
			}
				
			
			if (LogoAnimation.finished || (DestroyOnZero && Vector3.Distance(this.transform.position,Vector3.zero) <= speed/2))
			{
				LogoAnimation.finished = true;
				for(int i = 0; i < triggerOn.Count; i++)
				{
					triggerOn[i].SetActive(true);
				}
				for(int i = 0; i < triggerOff.Count; i++)
				{
					triggerOff[i].SetActive(false);
				}
				this.gameObject.SetActive(false);
			}
		}
	}
}
