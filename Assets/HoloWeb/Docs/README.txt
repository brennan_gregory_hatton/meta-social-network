HoloWeb 1.0.2

Description:
First Meta application that lets you browse the internet using Augmented Reality on Meta glasses.
Try the only augmented reality Web browser and surround yourself with data.

Important Notes:
    * This version is compatible with Meta SDK 1.1.0
    * ALT+F4 to quit application
    
Overview Video:
https://www.youtube.com/watch?v=wJ9bWmVglYE

Release Notes:

v1.0.2 - 20th February 2015

    Changes:
        * Code has been released as open source
    
    Fixes:
        * Physical keyboard and virtual keyboard can now be used interchangeably
        
    Known Issues:
        * Web videos may not be compatible with certain devices
        * Scrollbar rolls back to the top on webpages that have dynamically loading content
            (ex. Infinite scroll pages)
        * Tapping links or typing on the virutal keyboard may get registered as grabbing and cause web pages to stick to your palm


v1.0.1 - 24th November 2014:

    Fixes:
        * Changed to DX9 mode to avoid web pages not loading on some machines
        
    Known Issues:
        * Web videos may not be compatible with certain devices
        * Scrollbar rolls back to the top on webpages that have dynamically loading content
            (ex. Infinite scroll pages)
        * Non-virtual keyboard input will not work in the address bar
        * Tapping links or typing on the virutal keyboard may get registered as grabbing and cause web pages to stick to your palm

v1.0.0 - 21st November 2014:

    Known Issues:
        * Web videos may not be compatible with certain devices
        * Scrollbar rolls back to the top on webpages that have dynamically loading content
            (ex. Infinite scroll pages)
        * Non-virtual keyboard input will not work in the address bar
        * Tapping links or typing on the virutal keyboard may get registered as grabbing and cause web pages to stick to your palm
        
Copyright 2015 Meta Company.