﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using Coherent.UI;

namespace Meta.Apps.HoloWeb
{
    /// <summary>
    /// Adds Meta specific functions to a Coherent View, such as hand interactions.
    /// </summary>
    public class HWView : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        #region Member variables

        /// <summary>
        /// The Coherent engine's JavaScript file to allow calls between Unity and the javascript within the views.
        /// </summary>
        private string _coherentJS;

        /// <summary>
        /// Reference to the CoherentUIView
        /// </summary>
        [SerializeField]
        private CoherentUIView _view;
        /// <summary>
        /// Reference to the title gameobject
        /// </summary>
        [SerializeField]
        private Text _title;
        /// <summary>
        /// Reference to the address bar gameobject
        /// </summary>
        [SerializeField]
        private GameObject _addressBar;
        /// <summary>
        /// Reference to the vertical scroll bar gameobject
        /// </summary>
        [SerializeField]
        private Scrollbar _verticalScrollBar;

        /// <summary>
        /// The current target y pixel value that the page is being scrolled to.
        /// </summary>
        int _targetScrollY;
        /// <summary>
        /// The last y pixel value that the page was scrolled to
        /// </summary>
        int _lastScrollY;
        /// <summary>
        /// Whether the page has just been clicked on (so that the keyboard can be reopened after being manually closed)
        /// </summary>
        private bool _justClicked = false;
        /// <summary>
        /// The tag of the currently focussed HTML element in the page
        /// </summary>
        private string _tag;
        /// <summary>
        /// The last view path that was sucessfully loaded
        /// </summary>
        private string _lastViewPath;

        #endregion

        #region MonoBehaviour methods

        /// <summary>
        /// Use this for initialization
        /// </summary>
	    private void Start()
        {
            // Load the Coherent JS file
            TextAsset coherentJStext = Resources.Load("CoherentJS") as TextAsset;
            _coherentJS = coherentJStext.text;
            // Sets the coherent listeners
            SetListeners();
            // Keyboard input is disabled until we focus on an inputfield or textarea
            _view.ReceivesInput = false;
	    }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        private void Update()
        {
            if (_view.View != null)
            {
                SetAddressBar();
                UpdatePointerMove();
                ExecuteJavaScriptCalls();
                // If the page has not scrolled to the correct location according to the scrollbar
                _targetScrollY = (int)(_verticalScrollBar.value * ((_view.Height / _verticalScrollBar.size) - _view.Height));
                if (_targetScrollY != _lastScrollY)
                {
                    Scroll();
                }
            }
        }

        #endregion

        #region Member methods

        /// <summary>
        /// Sets the Coherent listeners for page start loading, finish loading and fail loading
        /// </summary>
        private void SetListeners()
        {
            // When we start loading a new page we reset the scrollbar
            _view.Listener.StartLoading += () =>
            {
                if (_lastViewPath != _view.View.GetCurentViewPath())
                {
                    _verticalScrollBar.value = 0;
                }
            };
            // When the page finishes loading we can inject the coherent engine JS file
            _view.Listener.FinishLoad += (frameId, validatedPath, isMainFrame, statusCode, headers) =>
            {
                if (isMainFrame)
                {
                    _view.View.ExecuteScript(_coherentJS);
                    _lastViewPath = _view.View.GetCurentViewPath();
                }
            };
            // If the address bar is not recognized as a page we will google what they typed
            _view.Listener.FailLoad += (frameId, validatedPath, isMainFrame, error) =>
            {
                if (isMainFrame)
                {
                	if (_addressBar != null)
	                	LoadPage("www.google.com/search?q=" + _addressBar.GetComponent<MGUIInputField>().text);//LoadPage("www.facebook.com");//
                }
            };
        }

        /// <summary>
        /// Executes javascript in the view to send variables such as the page title and currently active element
        /// </summary>
        private void ExecuteJavaScriptCalls()
        {
            // send the page title
            _view.View.ExecuteScript("engine.call('pageTitle', document.title)");
            // hide the html scrollbar
            _view.View.ExecuteScript("document.body.style.overflow='hidden';");
            // send the page height to set the scroll bar correctly
            _view.View.ExecuteScript("engine.call('SetScrollBar', document.body.scrollHeight, window.pageYOffset)");
            // send the currently focussed html element
            _view.View.ExecuteScript("var obj = document.activeElement; var curtop = 0; do { curtop += obj.offsetTop; } while (obj = obj.offsetParent); engine.call('GetInputField', document.activeElement.tagName.toLowerCase(), curtop-window.pageYOffset);");
        }

        /// <summary>
        /// sets the page title from the JS call
        /// </summary>
        [Coherent.UI.CoherentMethod("pageTitle")]
        private void OnPageTitleObtained(string title)
        {
            if (_title != null)
            {
                _title.text = title;
            }
        }

        /// <summary>
        /// sets the scrollbar size from the JS call
        /// </summary>
        [Coherent.UI.CoherentMethod("SetScrollBar")]
        private void SetScrollBar(int pageSize, int scrollPos)
        {
            if (_verticalScrollBar != null)
            {
                float newScrollbarSize = (float)_view.Height / (float)pageSize;
                if (_verticalScrollBar.size != newScrollbarSize)
                {
                    _verticalScrollBar.size = (float)_view.Height / (float)pageSize;
                    _verticalScrollBar.value = _targetScrollY / ((_view.Height / _verticalScrollBar.size) - _view.Height);
                }
            }
        }

        /// <summary>
        /// Checks whether we are currently focussed on an input or textarea and should open the keyboard
        /// </summary>
        [Coherent.UI.CoherentMethod("GetInputField")]
        private void GetInputField(string tag, int y)
        {
            _tag = tag;
            if ((tag == "input" || tag == "textarea"))
            {
                // if the focussed input or textarea is in the visible part of the page
                if (y >= 0 && y <= _view.Height)
                {
                    // calculate the y position of the input element
                    float yFloat = (float)y;
                    yFloat -= (float)_view.Height / (float)2;
                    yFloat /= (float)_view.Height;
                    yFloat = yFloat * 10 * transform.lossyScale.z;
                    HWManager.Instance.KeyboardFocus(gameObject, yFloat, _justClicked);
                    _justClicked = false;
                }
                else
                {
                    HideKeyboard();
                }
            }
            else
            {
                HideKeyboard();
            }
        }

        /// <summary>
        /// Hide the keyboard as this view no longer needs it
        /// </summary>
        private void HideKeyboard()
        {
            HWManager.Instance.KeyboardUnfocus(gameObject);
        }

        /// <summary>
        /// Send a key press to the view
        /// </summary>
        /// <param name="press">The key to press</param>
        public void PressKey(Event press)
        {
            if (press.keyCode == KeyCode.Backspace || press.keyCode == KeyCode.Tab || press.keyCode == KeyCode.Return)
            {
                // if this is a textarea the return key should create a new line
                if (press.keyCode == KeyCode.Return && _tag == "textarea")
                {
                    _view.View.KeyEvent(InputManager.ProcessCharEvent(press));
                }
                else
                {
                    KeyEventData keyEvent = InputManager.ProcessKeyEvent(press);
                    keyEvent.Type = KeyEventData.EventType.KeyDown;
                    _view.View.KeyEvent(keyEvent);
                    keyEvent.Type = KeyEventData.EventType.KeyUp;
                    _view.View.KeyEvent(keyEvent);
                }
            }
            else
            {
                _view.View.KeyEvent(InputManager.ProcessCharEvent(press));
            }
        }

        /// <summary>
        /// Scroll the window to patch the MGUI scrollbar.
        /// </summary>
        public void Scroll()
        {
            // lerp the scroll value to make it look more smooth
            _lastScrollY = (int)Mathf.Lerp(_lastScrollY, _targetScrollY, 0.1f);
            if (_view.View != null)
            {
                _view.View.ExecuteScript("window.scrollTo(0, " + _lastScrollY + ");");
            }
        }

        /// <summary>
        /// Set the text of the address bar to equal the current page
        /// </summary>
        private void SetAddressBar()
        {
            // if the panel is showing
            if (_addressBar != null && _addressBar.GetComponent<MGUIInputField>() != null && _addressBar.activeInHierarchy)
            {
                // if the address bar is the not currently focussed element
                if (MetaKeyboard.Instance.keyboardObject != _addressBar)
                {
                    if (_view.View.GetCurentViewPath() != null)
                    {
                        if (_addressBar.GetComponent<MGUIInputField>().text != _view.View.GetCurentViewPath())
                        {
                            _addressBar.GetComponent<MGUIInputField>().text = _view.View.GetCurentViewPath();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Load a new path
        /// </summary>
        /// <param name="path">The path to load</param>
        public void LoadPage(string path)
        {
            _view.View.Load(path);
        }

        /// <summary>
        /// Send the current finger pointer positions to the view in order to update the mouse hover effect
        /// </summary>
        private void UpdatePointerMove()
        {
            if (Hands.right != null)
            {
                if (Hands.right.isValid)
                {
                    MouseEvent(Hands.right.pointer.position, MouseEventData.EventType.MouseMove);
                }
                if (Hands.left.isValid)
                {
                    MouseEvent(Hands.left.pointer.position, MouseEventData.EventType.MouseMove);
                }
            }
        }

        /// <summary>
        /// Processes a pointer down event
        /// </summary>
        /// <param name="pointerEvent">The PointerEventData class of the pointer down event</param>
        public void OnPointerDown(PointerEventData pointerEvent)
        {
            if (!HWManager.Instance.IsNavigationMode())
            {
                // simulate a mouse down event
                MouseEvent(pointerEvent.worldPosition, MouseEventData.EventType.MouseDown);
                if (pointerEvent.pointerId != -1)
                {
                    OnPointerUp(pointerEvent);
                }
            }
        }

        /// <summary>
        /// Processes a pointer up event
        /// </summary>
        /// <param name="pointerEvent">The PointerEventData class of the pointer down event</param>
        public void OnPointerUp(PointerEventData pointerEvent)
        {
            if (!HWManager.Instance.IsNavigationMode())
            {
                if (GetComponent<AudioSource>() != null)
                {
                    GetComponent<AudioSource>().Play();
                }
                // Instantiate the finger pointer press indicator
                InputIndicators.Instance.InstantiatePressIndicator(pointerEvent.worldPosition, transform.rotation);
                // Simulate a MouseUp event
                MouseEvent(pointerEvent.worldPosition, MouseEventData.EventType.MouseUp);
                _justClicked = true;
            }

        }

        // Allow the physical keyboard input into this view
        public void ToggleKeyboardInput(bool enabled)
        {
            _view.ReceivesInput = enabled;
        }

        /// <summary>
        /// A simulated mouse click event, based on the position of the finger pointer
        /// </summary>
        /// <param name="pointerPos">The position of the finger pointer</param>
        /// <param name="eventType">Whether it's a mouseup or mousedown event</param>
        public void MouseEvent(Vector3 pointerPos, MouseEventData.EventType eventType)
        {
            if (_view.View != null)
            {
                MouseEventData mouse = new Coherent.UI.MouseEventData();
                mouse.Type = eventType;
                mouse.Button = MouseEventData.MouseButton.ButtonLeft;
                mouse.MouseModifiers = new EventMouseModifiersState();
                mouse.MouseModifiers.IsLeftButtonDown = (eventType == MouseEventData.EventType.MouseDown);

                RaycastHit rcast;
                Vector3 from = Camera.main.transform.position;
                Vector3 ray = Vector3.Normalize(pointerPos - Camera.main.transform.position);
                LayerMask mask = (1 << (int)Layers.UI) | (1 << (int)Layers.UIHUD);
                if (Physics.Raycast(from, ray, out rcast, Mathf.Infinity, mask))
                {
                    mouse.X = (int)(rcast.textureCoord.x * (float)_view.Width);
                    mouse.Y = _view.Height - (int)(rcast.textureCoord.y * (float)_view.Height);
                    _view.View.MouseEvent(mouse);
                }
            }
        }

        /// <summary>
        /// Scroll the page using the mouse scroll
        /// </summary>
        /// <param name="rawScrollValue">The raw scroll value</param>
        public void MouseScroll(float rawScrollValue)
        {
            float newScrollVal = _verticalScrollBar.value + _verticalScrollBar.size * (-rawScrollValue) * 5;
            _verticalScrollBar.value = newScrollVal;
        }

        /// <summary>
        /// Go back to the last loaded path
        /// </summary>
        public void Back()
        {
            ((BrowserView)_view.View).GoBack();
        }

        /// <summary>
        /// Go forward to a path before the back button was pressed
        /// </summary>
        public void Forward()
        {
            ((BrowserView)_view.View).GoForward();
        }

        /// <summary>
        /// Reload the current view path
        /// </summary>
        public void Refresh()
        {
            LoadPage(_view.View.GetCurentViewPath());
        }

        /// <summary>
        /// Create a new tab
        /// </summary>
        public void AddTab()
        {
            HWManager.Instance.AddTab();
        }

        /// <summary>
        /// Close this tab
        /// </summary>
        public void CloseTab()
        {
            HWManager.Instance.CloseTab(transform.parent.parent.gameObject);
        }

        #endregion
    }
}
