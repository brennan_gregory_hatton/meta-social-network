﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Meta.Apps.HoloWeb
{
    /// <summary>
    /// Tells a tab what to do when certain interactions are acted upon it, such as grabbing, touching and pointing.
    /// </summary>
    public class HWTab : MonoBehaviour
	{
		
        #region Member methods
		
		public Text TitleText;
		public GameObject canvas;
		public CoherentUIView coherentUIView;
		
		protected void Awake () {
			canvas = transform.FindChild("MGUI.Canvas").gameObject;
			TitleText = canvas.transform.FindChild("MGUI.Panel").FindChild("Title").gameObject.GetComponent<Text>();
			coherentUIView = transform.GetChild(0).FindChild("Window").GetComponent<CoherentUIView>();
			Debug.Log(TitleText);
			
		}
		
        /// <summary>
        /// Touch Dwelling on a tab during navigation mode will select it
        /// </summary>
        public void OnTouchDwell()
        {
            if (HWManager.Instance.IsNavigationMode())
            {
                HWManager.Instance.SelectTab(gameObject);
            }
        }

        /// <summary>
        /// Decrement the number of grabbed tabs
        /// </summary>
        public void OnRelease()
        {
            HWManager.Instance.HandleReleasedTab(gameObject);
        }

        /// <summary>
        /// Point Dwelling on a tab will start navigation mode
        /// </summary>
        public void OnPointDwell()
        {
            if (!HWManager.Instance.IsNavigationMode())
            {
                HWManager.Instance.StartNavigationMode();
            }
        }

        #endregion
    }
}
