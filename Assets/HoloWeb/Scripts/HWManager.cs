﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace Meta.Apps.HoloWeb
{
    /// <summary>
    /// Manages all the web tabs, such as navigating between them and moving them around.
    /// </summary>
    public class HWManager : MetaSingleton<HWManager>
    {
        #region Member variables
	    
	    public List<string> LoggedOutText = new List<string>();
	    public List<string> LoggedOutURL = new List<string>();
	    public List<string> LoggedInText = new List<string>();
	    public List<string> LoggedInURL = new List<string>();
	    
        /// <summary>
        /// Reference to the Tab prefab.
        /// </summary>
        [SerializeField]
        private GameObject _holoWebTab;
        /// <summary>
        /// Reference to the swipe canvas gameobject.
        /// </summary>
        [SerializeField]
        private GameObject _swipeCanvas;
        /// <summary>
        /// Reference to the content gameobject of the scrollview.
        /// </summary>
        [SerializeField]
        private RectTransform _scrollViewContent;
        
        /// <summary>
        /// List of web pages to load on start.
        /// </summary>
        [SerializeField]
	    private List<string> _startPages;
        /// <summary>
        /// The current tab at the front.
        /// </summary>
	    public HWTab _frontTab;
	    public GameObject frontTabObj;
	    private Text frontTabelTitle;
        /// <summary>
        /// The current tab at the front.
        /// </summary>
	    public List<SecondaryTab> SecondaryTabs = new List<SecondaryTab>();
        /// <summary>
        /// All tabs that are being managed (not including those that are in unmanaged mode).
        /// </summary>
        private List<GameObject> _tabs = new List<GameObject>();

        /// <summary>
        /// Whether we are in navigation mode.
        /// </summary>
        private bool _navigationMode = false;
        /// <summary>
        /// The current tab that the user is looking at.
        /// </summary>
        private GameObject _gazeTab;
        /// <summary>
        /// The current view that the keyboard is typing into.
        /// </summary>
        private GameObject _keyboardView;

        /// <summary>
        /// The previous value of the infinitive scroller.
        /// </summary>
        private float _prevSwipeValue;
        /// <summary>
        /// The current tilt angle of the pages in navigation mode.
        /// </summary>
        private float _tiltAngle = 0;
        /// <summary>
        /// Whether all tabs have been set to not-orbital in navigation mode.
        /// </summary>
        private bool _noOrbitalTabs = false;
        /// <summary>
        /// The current hand that is allowed to grab tabs.
        /// </summary>
        private HandType _grabbingHandType = HandType.EITHER;

        #endregion

        #region MonoBehaviour methods

        /// <summary>
        /// Use this for initialization.
        /// </summary>
        private void Start()
        {
            MetaMouse.Instance.enableMetaMouse = true;
	        InstantiateInitialTabs();
	        
	        
			//decapitalize url and heading
	        for(int i = 0; i < LoggedInText.Count; i++)
	        {
	        	LoggedInText[i] = LoggedInText[i].ToLower();
	        }
	        for(int i = 0; i < LoggedOutText.Count; i++)
	        {
	        	LoggedOutText[i] = LoggedOutText[i].ToLower();
	        }
	        for(int i = 0; i < LoggedInURL.Count; i++)
	        {
	        	LoggedInURL[i] = LoggedInURL[i].ToLower();
	        }
	        for(int i = 0; i < LoggedOutURL.Count; i++)
	        {
	        	LoggedOutURL[i] = LoggedOutURL[i].ToLower();
	        }
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        private void Update()
        {
            if (_navigationMode)
            {
                Navigate();
            }
            else
            {
                UpdateMouse();
	            CheckPalmVisibility();
	            
	            CheckIfLoggedOut();
            }
        }

        #endregion

        #region Member methods
	    
	    //*Added by Brennan*
	    //
	    private void CheckIfLoggedOut()
	    {
	    	
	    	//if main tab . title . text = "Welcome to Facebook"
		    if (LoggedOutText.Contains(_frontTab.TitleText.text.ToLower()) || (_frontTab.coherentUIView.View != null && LoggedOutURL.Contains(_frontTab.coherentUIView.View.GetCurentViewPath().ToLower())))
		    {
				//Disablewww.facebook.com/notifications
			    for(int i= 0; i < SecondaryTabs.Count; i++)
			    	SecondaryTabs[i].canvas.SetActive(false);
		    }
		    else if (LoggedInText.Contains(_frontTab.TitleText.text.ToLower())  || (_frontTab.coherentUIView.View != null && LoggedInURL.Contains(_frontTab.coherentUIView.View.GetCurentViewPath().ToLower())))
		    {
			    //Enable
			    for(int i= 0; i < SecondaryTabs.Count; i++)
			    {
			    	if (SecondaryTabs[i].canvas.active == false)
			    	{
				    	SecondaryTabs[i].canvas.SetActive(true);
			    		SecondaryTabs[i].Refresh();
			    	}
			    }
		    }
	    }

        /// <summary>
        /// Creates the tabs with the correct starting web pages and sets their metabodies correctly
        /// </summary>
        private void InstantiateInitialTabs()
        {
            // Initially the front tab is the middle one
            int frontTabIndex = (_startPages.Count - 1) / 2;
            for (int i = 0; i < _startPages.Count; i++)
            {
                // Instantiate the tab
                GameObject tab = (GameObject)Instantiate(_holoWebTab, Vector3.zero, Quaternion.identity);
                // set its parent to the me (the manager)
                tab.transform.parent = transform;
                // the number of tabs between this tab and the front (middle) tab
                int diff = i - frontTabIndex;
                tab.transform.localPosition = new Vector3(diff * 0.4f, 0, (Mathf.Abs(diff) + 1) * 0.4f);
                // Set the page to the initial page
                tab.transform.GetChild(0).FindChild("Window").GetComponent<CoherentUIView>().Page = _startPages[i];
                // add this tab to the list
                _tabs.Add(tab);
                if (i == frontTabIndex)
                {
                    SetToFrontTab(tab);
                }
                else
                {
                    SetToNonFrontTab(tab);
                }
            }
        }

        /// <summary>
        /// Sets the correct parameters for the front tab
        /// </summary>
        /// <param name="tab">The front tab</param>
        private void SetToFrontTab(GameObject tab)
        {
	        frontTabObj = tab;
	        _frontTab = frontTabObj.GetComponent<HWTab>();
            tab.GetComponent<Collider>().enabled = false;
            MetaBody metaBody = tab.GetComponent<MetaBody>();
            metaBody.pointable = false;
            metaBody.touchable = false;
            metaBody.grabbable = true;
            metaBody.orbital = true;
            tab.transform.GetChild(0).FindChild("MGUI.Panel").gameObject.SetActive(true);
            tab.transform.GetChild(0).FindChild("MGUI.Panel").FindChild("New Tab").gameObject.SetActive(true);
            if (_tabs.Count == 1)
            {
                // if this is the only tab, it should not be grabbable
                tab.GetComponent<MetaBody>().grabbable = false;
            }
        }

        /// <summary>
        /// Sets the correct parameters for a non-front tab
        /// </summary>
        /// <param name="tab">the non-front tab</param>
        private void SetToNonFrontTab(GameObject tab)
        {
            tab.GetComponent<Collider>().enabled = true;
            MetaBody metaBody = tab.GetComponent<MetaBody>();
            metaBody.pointable = true;
            metaBody.touchable = false;
            metaBody.grabbable = true;
            metaBody.orbital = true;
            tab.transform.GetChild(0).FindChild("MGUI.Panel").gameObject.SetActive(false);
        }

        /// <summary>
        /// Sets the correct parameters for a navigation tab
        /// </summary>
        /// <param name="tab">The navigation tab</param>
        private void SetToNavigationTab(GameObject tab)
        {
            tab.GetComponent<Collider>().enabled = true;
            MetaBody metaBody = tab.GetComponent<MetaBody>();
            metaBody.pointable = false;
            metaBody.touchable = true;
            metaBody.grabbable = false;
            tab.transform.GetChild(0).FindChild("MGUI.Panel").gameObject.SetActive(false);
        }

        /// <summary>
        /// Updates the tab that the user is looking at
        /// </summary>
        private void UpdateGazeTab()
        {
            GameObject gazeTarget = Gaze.Instance.objectOfInterest;
            // depending on which collider the gaze has hit, it could be two or three levels down from the tab
            if (gazeTarget != null && gazeTarget.transform.parent != null && gazeTarget.transform.parent.parent != null)
            {
                if (gazeTarget.transform.parent.parent.GetComponent<HWTab>() != null)
                {
                    _gazeTab = gazeTarget.transform.parent.parent.gameObject;
                }
                else if(gazeTarget.transform.parent.parent.parent != null &&
                    gazeTarget.transform.parent.parent.parent.GetComponent<HWTab>() != null)
                {
                    _gazeTab = gazeTarget.transform.parent.parent.parent.gameObject;
                }
            }
        }

        /// <summary>
        /// Starts the navigation mode and puts all the tabs in the correct position and correct scale
        /// </summary>
        public void StartNavigationMode()
        {
            _navigationMode = true;
            int frontTabIndex = _tabs.IndexOf(frontTabObj);
            GameObject temp = new GameObject();
            temp.transform.parent = transform;
            foreach (GameObject tab in _tabs)
            {
                int diff = _tabs.IndexOf(tab) - frontTabIndex;
                temp.transform.localPosition = new Vector3(0, 0, 0.4f);
                temp.transform.RotateAround(Camera.main.transform.position, Vector3.up, 10 * diff);
                LeanTween.scale(tab, new Vector3(0.33f, 0.33f, 0.33f), 0.5f);
                LeanTween.moveLocal(tab, temp.transform.localPosition, 0.5f);
                LeanTween.rotateLocal(tab, temp.transform.localRotation.eulerAngles, 0.5f);
                SetToNavigationTab(tab);
            }
            // Disable the unmanaged tabs while in navigation mode
            ToggleUnmanagedTabs(false);
            Destroy(temp);
            // enable the infinitive scroller
            _swipeCanvas.SetActive(true);
            _prevSwipeValue = _scrollViewContent.localPosition.x;
            // If any tab was using the keyboard, we should close the keyboard
            MetaKeyboard.Instance.ReleaseKeyboard(gameObject);
            _keyboardView = null;
	        _frontTab = null;
	        frontTabObj = null;
        }

        /// <summary>
        /// Scroll between the tabs using the infinitive scroller
        /// </summary>
        private void Navigate()
        {
            if (!MakeAllTabsNotOrbital())
            {
                return;
            }
            else
            {
                float newTiltAngle = 0;
                // if we can see at least one of the tabs
                if (MetaArrow.Instance.IsVisible(transform))
                {
                    if (_scrollViewContent.parent.GetComponent<MGUIScrollRect>() == null)
                    {
                        _scrollViewContent.parent.gameObject.AddComponent<MGUIScrollRect>();
                        _scrollViewContent.parent.GetComponent<MGUIScrollRect>().content = _scrollViewContent;
                        _scrollViewContent.parent.GetComponent<MGUIScrollRect>().vertical = false;
                        _scrollViewContent.parent.GetComponent<MGUIScrollRect>().movementType = MGUIScrollRect.MovementType.Unrestricted;
                    }
                    _scrollViewContent.parent.gameObject.SetActive(true);
                    _scrollViewContent.parent.GetComponent<Collider>().enabled = true;
                    if (_prevSwipeValue != _scrollViewContent.localPosition.x)
                    {
                        // scroll through tabs based on movement of the scroll view content
                        // and calculate the new tilt angle for the tabs
                        newTiltAngle = ScrollThroughTabs();
                    }
                }
                else
                {
                    // if we don't see any tabs disable the scroller
                    _scrollViewContent.parent.GetComponent<Collider>().enabled = false;
                    newTiltAngle = 0;
                }
                if (_tiltAngle != newTiltAngle)
                {
                    foreach (GameObject tab in _tabs)
                    {
                        // give the tabs a tilt effect when they are being scrolled through
                        tab.transform.Rotate(new Vector3(0, newTiltAngle - _tiltAngle, 0));
                    }
                    _tiltAngle = newTiltAngle;
                }
            }
        }

        /// <summary>
        /// Checks that no tabs are orbital so that they can be tilted and do not face camera
        /// </summary>
        /// <returns>Whether all tabs set to orbital = false</returns>
        private bool MakeAllTabsNotOrbital()
        {
            if (!_noOrbitalTabs)
            {
                foreach (GameObject tab in _tabs)
                {
                    // if the tab has not finished the LeanTween.Scale yet
                    if (tab.transform.localScale.x > 0.33001f)
                    {
                        return false;
                    }
                }
                foreach (GameObject tab in _tabs)
                {
                    tab.GetComponent<MetaBody>().orbital = false;
                    tab.transform.LookAt(Camera.main.transform);
                    tab.transform.Rotate(new Vector3(0, 180, 0));
                }
                // all tabs have been unset from orbital so that we can tilt them
                _noOrbitalTabs = true;
            }
            return true;
        }

        /// <summary>
        /// Scroll through tabs based on movement of the scroll view content
        /// and calculate the new tilt angle for the tabs
        /// </summary>
        /// <returns>The new tilt angle for the tabs</returns>
        private float ScrollThroughTabs()
        {
            float newTiltAngle = 0;
            float movement = (_scrollViewContent.localPosition.x - _prevSwipeValue);
            transform.Rotate(Vector3.up, movement / 30);
            // if we can no longer see the tabs after the last scroll movement we will "bounce back"
            if (!MetaArrow.Instance.IsVisible(transform))
            {
                float angle = 10;
                // the bounce will be opposite of the last movement
                if (movement > 0)
                {
                    angle = -angle;
                }
                transform.Rotate(Vector3.up, -movement / 30);
                LeanTween.rotateAround(gameObject, Vector3.up, angle, 0.5f).setEase(LeanTweenType.easeOutExpo);
                // Destroy the scroll rect to stop the scrolling
                Destroy(_scrollViewContent.parent.GetComponent<MGUIScrollRect>());
            }
            else
            {
                newTiltAngle = Mathf.Lerp(_tiltAngle, movement/2, Time.deltaTime * 10);
            }
            _prevSwipeValue = _scrollViewContent.localPosition.x;
            return newTiltAngle;
        }

        /// <summary>
        /// End navigation mode and select a tab to be the front tab
        /// </summary>
        /// <param name="frontTab">The tab to become the front tab</param>
        public void SelectTab(GameObject frontTab)
        {
            SetToFrontTab(frontTab);
            _navigationMode = false;
            _noOrbitalTabs = false;
            _swipeCanvas.SetActive(false);
            LeanTween.moveLocal(frontTabObj, new Vector3(0, 0, 0.4f), 0.5f);
            // calculate the angel between the camera and this gameobject
            float angleBetween = Camera.main.transform.rotation.eulerAngles.y - transform.rotation.eulerAngles.y;
            if (angleBetween > 180)
            {
                angleBetween -= 360;
            }
            else if (angleBetween < -180)
            {
                angleBetween += 360;
            }
            // rotate so that the front tab is at the middle of the view
            LeanTween.rotateAround(gameObject, Vector3.up, angleBetween, 0.5f);
            foreach (GameObject tab in _tabs)
            {
                // scale the tabs back to normal size
                LeanTween.scale(tab, new Vector3(1, 1, 1), 0.5f);
                int frontTabIndex = _tabs.IndexOf(frontTabObj);
                if (tab != frontTabObj)
                {
                    SetToNonFrontTab(tab);
                    int diff = _tabs.IndexOf(tab) - frontTabIndex;
                    LeanTween.moveLocal(tab, new Vector3(diff * 0.4f, 0, Mathf.Abs(diff) * 0.4f + 0.4f), 0.5f);
                }
            }
            // Re-enable any unmanaged tabs
            ToggleUnmanagedTabs(true);
        }

        /// <summary>
        /// Toggle the unmanaged tabs
        /// </summary>
        /// <param name="toggle">Whether they should be enabled</param>
        private void ToggleUnmanagedTabs(bool toggle)
        {
            foreach (Transform child in transform)
            {
                if (child.GetComponent<HWTab>() && !_tabs.Contains(child.gameObject))
                {
                    child.gameObject.SetActive(toggle);
                }
            }
        }

        /// <summary>
        /// Whether we are currently in navigation mode
        /// </summary>
        public bool IsNavigationMode()
        {
            return _navigationMode;
        }

        /// <summary>
        /// Create a new tab and make space for it
        /// </summary>
        public void AddTab()
        {
            // Close the keyboard if it was being used by the previous front tab
            MetaKeyboard.Instance.ReleaseKeyboard(gameObject);
            _keyboardView = null;
            SetToNonFrontTab(frontTabObj);
            GameObject tab = (GameObject)Instantiate(_holoWebTab, Vector3.zero, Quaternion.identity);
            tab.transform.parent = transform;
            tab.transform.localPosition = new Vector3(0, 0, 0.4f);
            // Make the new tab appear to expand from the center
            tab.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            LeanTween.scale(tab, new Vector3(1, 1, 1), 0.5f);
            int frontTabIndex = _tabs.IndexOf(frontTabObj);
            _tabs.Insert(frontTabIndex, tab);
            SetToFrontTab(tab);
	        MoveTabs(frontTabIndex);
	        
	        // Set the page to the initial page - THis is now done in HWTab
	        tab.transform.GetChild(0).FindChild("Window").GetComponent<CoherentUIView>().Page = _startPages[0];
        }

        /// <summary>
        /// Close a tab and move another tab into its position if necessary
        /// </summary>
        /// <param name="tab">the tab to close</param>
        public void CloseTab(GameObject tab)
        {
            // Close the keyboard if it was being used by tab being closed
            MetaKeyboard.Instance.ReleaseKeyboard(gameObject);
            _keyboardView = null;
            // if we are closing the front tab
            if (tab == frontTabObj)
            {
                // If we have more than one tab open
                if (_tabs.Count > 1)
                {
                    int frontTabIndex = _tabs.IndexOf(tab);
                    // give it the appearance of shrinking into itself
                    LeanTween.scale(tab, new Vector3(0, 0, 0), 0.5f);
                    _tabs.Remove(tab);
                    StartCoroutine("DestroyTab", tab);
                    // move all the other managed tabs into place and set a new front tab
                    if (_tabs.Count > frontTabIndex)
                    {
	                    frontTabObj = _tabs[frontTabIndex];
	                    _frontTab = frontTabObj.GetComponent<HWTab>();
                        for (int i = frontTabIndex; i < _tabs.Count; i++)
                        {
                            LeanTween.moveLocal(_tabs[i], new Vector3((i - frontTabIndex) * 0.4f, 0, (i - frontTabIndex) * 0.4f + 0.4f), 0.5f);
                        }
                    }
                    else
                    {
	                    frontTabObj = _tabs[frontTabIndex - 1];
	                    _frontTab = frontTabObj.GetComponent<HWTab>();
                        for (int i = 0; i < frontTabIndex; i++)
                        {
                            LeanTween.moveLocal(_tabs[i], new Vector3((frontTabIndex - 1 - i) * -0.4f, 0, (frontTabIndex - 1 - i) * 0.4f + 0.4f), 0.5f);
                        }
                    }
                    SetToFrontTab(frontTabObj);
                }
            }
            // if we are closing an unmanaged tab
            else
            {
                // give it the appearance of shrinking into itself
                LeanTween.scale(tab, new Vector3(0, 0, 0), 0.5f);
                StartCoroutine("DestroyTab", tab);
            }
        }

        /// <summary>
        /// Destroy a tab's gameobject after waiting half a second
        /// </summary>
        /// <param name="tab">the tab to destroy</param>
        private IEnumerator DestroyTab(GameObject tab)
        {
            yield return new WaitForSeconds(0.5f);
            Destroy(tab);
        }

        /// <summary>
        /// Make a grabbed and released tab unmanaged or bring a unmanaged tab back into the managed tabs
        /// </summary>
        /// <param name="tab">The released tab</param>
        public void HandleReleasedTab(GameObject tab)
        {
            GameObject temp = new GameObject();
            temp.transform.parent = transform;
            temp.transform.localPosition = new Vector3(0, 0, 0.4f);
            // if this is a managed tab
            if (_tabs.Contains(tab))
            {
                // if it was moved less than 0.15f away from its original position
                if (Vector3.Distance(temp.transform.position, tab.transform.position) <= 0.15f)
                {
                    // put it back in the front tab position
                    LeanTween.moveLocal(tab, new Vector3(0, 0, 0.4f), 0.5f);
                }
                // if it was moved further than 0.15f away from its original position
                else
                {
                    MakeUnmanagedTab(tab);
                }
            }
            // if this is an unmanaged tab
            else
            {
                // if the tab is released less than 0.15f from the front tab position
                if (Vector3.Distance(temp.transform.position, tab.transform.position) <= 0.15f)
                {
                    // move it to the front tab position and move the other tabs out of the way
                    LeanTween.moveLocal(tab, new Vector3(0, 0, 0.4f), 0.5f);
                    int frontTabIndex = _tabs.IndexOf(frontTabObj);
                    _tabs.Insert(frontTabIndex, tab);
                    SetToNonFrontTab(frontTabObj);
                    SetToFrontTab(tab);
                    MoveTabs(frontTabIndex);
                }
            }
            Destroy(temp);
        }

        /// <summary>
        /// Removes a tab from the managed tabs and makes it a unmanaged tab
        /// </summary>
        /// <param name="tab">The tab that will become unmanaged</param>
        private void MakeUnmanagedTab(GameObject tab)
        {
            // if there is more than one managed tab
            if (_tabs.Count > 1)
            {
                int frontTabIndex = _tabs.IndexOf(tab);
                _tabs.Remove(tab);
                // move all the other managed tabs into place and set a new front tab
                if (_tabs.Count > frontTabIndex)
                {
	                frontTabObj = _tabs[frontTabIndex];
	                _frontTab = frontTabObj.GetComponent<HWTab>();
                    for (int i = frontTabIndex; i < _tabs.Count; i++)
                    {
                        LeanTween.moveLocal(_tabs[i], new Vector3((i - frontTabIndex) * 0.4f, 0, (i - frontTabIndex) * 0.4f + 0.4f), 0.5f);
                    }
                }
                else
                {
	                frontTabObj = _tabs[frontTabIndex - 1];
	                _frontTab = frontTabObj.GetComponent<HWTab>();
                    for (int i = 0; i < frontTabIndex; i++)
                    {
                        LeanTween.moveLocal(_tabs[i], new Vector3((frontTabIndex - 1 - i) * -0.4f, 0, (frontTabIndex - 1 - i) * 0.4f + 0.4f), 0.5f);
                    }
                }
                SetToFrontTab(frontTabObj);
                // unmanaged tabs can not create new tabs
                tab.transform.GetChild(0).FindChild("MGUI.Panel").FindChild("New Tab").gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// Move tabs to make room for a newly created front tab or a unmanaged tab becoming a front managed tab
        /// </summary>
        /// <param name="frontTabIndex">The index of the front tab</param>
        private void MoveTabs(int frontTabIndex)
        {
            for (int x = frontTabIndex + 1; x < _tabs.Count; x++)
            {
                LeanTween.moveLocal(_tabs[x], new Vector3(_tabs[x].transform.localPosition.x + 0.4f, 0, _tabs[x].transform.localPosition.z + 0.4f), 0.5f);
            }
        }

        /// <summary>
        /// Checks if the palm is visible in order to set which hand (if any) is allowed to grab
        /// </summary>
        private void CheckPalmVisibility()
        {
            HandType grabbingHandType = HandType.UNKNOWN;
            bool leftPalmVisible = false;
            bool rightPalmVisible = false;
            if (Hands.left != null)
            {
                leftPalmVisible = MetaCamera.IsVisible(Hands.left.palm.gameObject.transform.GetComponent<Collider>().bounds);
                rightPalmVisible = MetaCamera.IsVisible(Hands.right.palm.gameObject.transform.GetComponent<Collider>().bounds);
            }
            if (leftPalmVisible && rightPalmVisible)
            {
                grabbingHandType = HandType.EITHER;
            }
            else if (leftPalmVisible)
            {
                grabbingHandType = HandType.LEFT;
            }
            else if (rightPalmVisible)
            {
                grabbingHandType = HandType.RIGHT;
            }
            if (_grabbingHandType != grabbingHandType)
            {
                foreach (Transform child in transform)
                {
                    if (child.GetComponent<HWTab>() && child.GetComponent<MetaBody>())
                    {
                        child.GetComponent<MetaBody>().grabbingHandType = grabbingHandType;
                    }
                }
                _grabbingHandType = grabbingHandType;
            }
        }

        /// <summary>
        /// Open and set the position of the keyboard to one of the views
        /// </summary>
        /// <param name="window">The view which requested the keyboard</param>
        /// <param name="y">The y position of the keyboard</param>
        /// <param name="justClicked">Whether this view was just clicked on
        /// (and hence should override an input field's use of the keyboard)</param>
        public void KeyboardFocus(GameObject window, float y, bool justClicked = false)
        {
            UpdateGazeTab();
            GameObject tab = window.transform.parent.parent.gameObject;
            // if the user is looking at the tab and it is the front tab or an unmanaged tab
            if (tab == _gazeTab &&
                (tab == frontTabObj ||
                !_tabs.Contains(tab)))
            {
                if (_keyboardView != window || justClicked)
                {
                    _keyboardView = window;
                    MetaKeyboard.Instance.RequestKeyboard(gameObject);
                    // allow the physical keyboard to input into the view
                    window.GetComponent<HWView>().ToggleKeyboardInput(true);
                }
                // set the position of the keyboard to be slightly under and in front of input field on the view
                Vector3 pos = window.transform.position + window.transform.forward * (0.02f + y) + window.transform.up * 0.01f;
                MetaKeyboard.Instance.SetKeyboardPosition(gameObject, pos);
                GameObject temp = new GameObject();
                temp.transform.rotation = window.transform.rotation;
                temp.transform.Rotate(90, 180, 0);
                MetaKeyboard.Instance.SetKeyboardRotation(gameObject, temp.transform.rotation);
                Destroy(temp);
            }
        }

        /// <summary>
        /// Close the keyboard when it is no longer required by the views
        /// </summary>
        /// <param name="window">The view that no longer requires the keyboard</param>
        public void KeyboardUnfocus(GameObject window)
        {
            UpdateGazeTab();
            GameObject tab = window.transform.parent.parent.gameObject;
            // if the user is looking at the tab and it is the front tab or an unmanaged tab
            if (tab == _gazeTab &&
                (tab == frontTabObj ||
                !_tabs.Contains(tab)))
            {
                _keyboardView = null;
                MetaKeyboard.Instance.ReleaseKeyboard(gameObject);
                // disallow the physical keyboard to input into the view
                window.GetComponent<HWView>().ToggleKeyboardInput(false);
            }
        }

        /// <summary>
        /// Send a key press to the appropriate view
        /// </summary>
        /// <param name="press">The event of the key press</param>
        public void PressKey(Event press)
        {
            // if there is a view which is using the keyboard
            if (_keyboardView != null)
            {
                _keyboardView.GetComponent<HWView>().PressKey(press);
            }
        }

        /// <summary>
        /// If the MetaMouse is enabled and is over a view, send a MouseMove MouseEvent to the tab
        /// </summary>
        private void UpdateMouse()
        {
            if (MetaMouse.Instance.enableMetaMouse)
            {
                if (MetaMouse.Instance.objectOfInterest != null &&
                    MetaMouse.Instance.objectOfInterest.GetComponent<HWView>() != null)
                {
                    HWView view = MetaMouse.Instance.objectOfInterest.GetComponent<HWView>();
                    view.MouseEvent(MetaMouse.Instance.worldPosition, Coherent.UI.MouseEventData.EventType.MouseMove);

                    float rawScrollValue = Input.GetAxis("Mouse ScrollWheel");
                    if (rawScrollValue != 0)
                    {
                        view.MouseScroll(rawScrollValue);
                    }
                }
            }
        }

        #endregion
    }
}
