﻿using UnityEngine;
using System.Collections;

namespace Meta.Apps.HoloWeb
{
    /// <summary>
    /// Controls what happens when the user submits the address bar
    /// </summary>
    public class HWSubmitAddressBar : MonoBehaviour
    {
        #region Member methods

        /// <summary>
        /// Tells the HWView to load a new path when the address bar is submitted
        /// </summary>
        /// <param name="path">The path to load</param>
        public void Submit(string path)
        {
            transform.parent.parent.FindChild("Window").GetComponent<HWView>().LoadPage(path);
        }

        #endregion
    }
}
