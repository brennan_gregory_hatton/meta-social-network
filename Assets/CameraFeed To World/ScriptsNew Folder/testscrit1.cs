﻿using UnityEngine;
using System.Collections;
using Meta;

public class testscrit1 : MonoBehaviour {
	
	int sourceDevice;
	Texture2D tex;
	Renderer renderer;
	
	// Use this for initialization
	void Start () {
		sourceDevice = 0;
		renderer = this.GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
		tex = DeviceTextureSource.Instance.GetDeviceTexture(sourceDevice);
		
		renderer.material.mainTexture = tex;
	}
}
