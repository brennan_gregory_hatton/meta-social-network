﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Meta.Apps.HoloWeb;

public class SecondaryTab : HWTab {
	
	public string defaultAddress;
	//private CoherentUIView coherentUIView;
	public HWView windowView;
	
	public bool ClicksToNewTab = false;
	public bool pageToOpen = false;
	public string addressOpenning;
	// Use this for initialization
	
	void Awake()
	{
		base.Awake();
		
		//coherentUIView = transform.GetChild(0).FindChild("Window").GetComponent<CoherentUIView>();
		coherentUIView.Page = defaultAddress;
		windowView = canvas.transform.FindChild("Window").GetComponent<HWView>();
		
	}
	
	
	// Update is called once per frame
	void Update () {
		
		//CheckIfLoggedOut();
		
		if (ClicksToNewTab)
		{
			//TUrn page update into new tab
			//PageUpdateToTab();
		
		}
		
	}
	
	void PageUpdateToTab()
	{
		if (coherentUIView.View == null)
			return;
		
		Debug.Log(coherentUIView.View.GetCurentViewPath());
		//Debug.Log(defaultAddress);
		
		//Check if page is not default
		if (pageToOpen == false && coherentUIView.View.GetCurentViewPath() != null && coherentUIView.View.GetCurentViewPath() != defaultAddress)
		{
			//save tab
			pageToOpen = true;
			addressOpenning = coherentUIView.View.GetCurentViewPath();
			
			//change url back to default
			coherentUIView.Page = defaultAddress;//windowView.LoadPage(defaultAddress);
			
		}
		//When tab has changed back
		else if (pageToOpen && coherentUIView.View.GetCurentViewPath() == defaultAddress)
		{
			
			//Open new tab with saved url
			HWManager.Instance.AddTab();
			HWManager.Instance.frontTabObj.transform.GetChild(0).FindChild("Window").gameObject.GetComponent<HWView>().LoadPage(addressOpenning);
			pageToOpen = false;
		}//*/
	}
	
	public void Refresh()
	{
		//coherentUIView.Page = defaultAddress;
		windowView.LoadPage(defaultAddress);
	}
	
	/*private void CheckIfLoggedOut()
	{
			
	    	//if main tab . title . text = "Welcome to Facebook"
		if (TitleText.text == "Welcome to Facebook")
		{
			//Disable
			if (canvas.active)
				canvas.SetActive(false);
		}
		else
		{
			
			if (!canvas.active)
			//enable
				canvas.SetActive(true);
		}
	}*/
}
